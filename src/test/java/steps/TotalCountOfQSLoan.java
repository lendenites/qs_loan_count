package steps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class TotalCountOfQSLoan 
{

	WebDriver driver;
	String Count="";

    
	String MonitoringData[]= {"Status","SuccessCount","Run Date & Time"};
	String MonitoringCol[]= {"TotalLoanCountOfQS","",""};
    String MonitoringRow[]= {"",""};

	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTg4LCJmaWx0ZXIiOlsiYW5kIixbInRpbWUtaW50ZXJ2YWwiLFsiZmllbGQtaWQiLDQxN10sLTEsImRheSIse31dLFsiPSIsWyJmaWVsZC1pZCIsNDE0XSwiUVMiXV0sImFnZ3JlZ2F0aW9uIjpbWyJjb3VudCJdXX0sImRhdGFiYXNlIjoyfSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	public String allCount[]= {"Count","null"};
	List<String[]> data;
	String rowSpace [] = {null};
	public TotalCountOfQSLoan(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception
	{   
		this.data=data;
		this.driver=driver;
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringCol[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
        MonitoringCol[2]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).getText();
        Thread.sleep(2000);
		writer.writeNext(MonitoringData);
		writer.writeNext(MonitoringRow);
		writer.writeNext(MonitoringCol);
		data.add(rowSpace);
		writer.flush();
}

}
