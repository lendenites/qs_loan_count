package steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

import webCapability.DateAndTime;

public class ListedLoanCount 
{
	WebDriver driver;
	CSVWriter writer;
	String Count="";
	List<String[]> data;
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE4OCwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiw0MTRdLCJRUyJdLFsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIixbImZpZWxkLWlkIiwxMzE4XV0sIkxJU1RFRCJdLFsiYmV0d2VlbiIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIixbImZpZWxkLWlkIiwxMzIwXV0sIjIwMjItMDMtMTYiLCIyMDIyLTAzLTE3Il1dLCJqb2lucyI6W3siZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxNTgsImNvbmRpdGlvbiI6WyI9IixbImZpZWxkLWlkIiw0MDZdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEN1c3RvbXVzZXIiLFsiZmllbGQtaWQiLDc0OF1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgQ3VzdG9tdXNlciJ9LHsiZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxMTEsImNvbmRpdGlvbiI6WyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBDdXN0b211c2VyIixbImZpZWxkLWlkIiw3NTFdXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBSZXF1aXJlZGxvYW4iLFsiZmllbGQtaWQiLDEyOTRdXV0sImFsaWFzIjoiTGVuZGVuYXBwIFJlcXVpcmVkbG9hbiJ9XSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwidHlwZSI6InF1ZXJ5In0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
	String MonitoringData[]= {"Listed","Count","Date and Time"};
	DateAndTime setTime=new DateAndTime();
	String rowSpace [] = {null,null};

	public ListedLoanCount(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception
	{
		this.data=data;
		this.driver=driver;	
		Thread.sleep(2000);
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(10));
		Thread.sleep(2000);
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringData[1]= getSucsessEnashUser();
		MonitoringData[2]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).getText();
		writer.writeNext(MonitoringData);
		writer.flush();
		data.add(rowSpace);
		data.add(rowSpace);
	}
	private String getSucsessEnashUser() {
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;

	}
}
