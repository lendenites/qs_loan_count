package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class DisbursedLoanCount 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]> data;
	String MonitoringData[]= {"DisbursedLoanCount","",""};
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTg4LCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiZmllbGQtaWQiLDQxNF0sIlFTIl0sWyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBSZXF1aXJlZGxvYW4iLFsiZmllbGQtaWQiLDEzMThdXSwiRElTQlVSU0VEIl0sWyJ0aW1lLWludGVydmFsIixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBSZXF1aXJlZGxvYW4iLFsiZmllbGQtaWQiLDEzMjBdXSwtMSwiZGF5Iix7fV1dLCJqb2lucyI6W3siZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxNTgsImNvbmRpdGlvbiI6WyI9IixbImZpZWxkLWlkIiw0MDZdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEN1c3RvbXVzZXIiLFsiZmllbGQtaWQiLDc0OF1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgQ3VzdG9tdXNlciJ9LHsiZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxMTEsImNvbmRpdGlvbiI6WyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBDdXN0b211c2VyIixbImZpZWxkLWlkIiw3NTFdXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBSZXF1aXJlZGxvYW4iLFsiZmllbGQtaWQiLDEyOTRdXV0sImFsaWFzIjoiTGVuZGVuYXBwIFJlcXVpcmVkbG9hbiJ9XSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwiZGF0YWJhc2UiOjJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	public DisbursedLoanCount(WebDriver driver,CSVWriter writer,List<String[]> data) throws IOException, InterruptedException
	{
		this.driver=driver;
		this.data=data;
		Thread.sleep(2000);
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(4));
		Thread.sleep(2000);
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		Thread.sleep(2000);
		writer.writeNext(MonitoringData);
		writer.flush();
	}
}
