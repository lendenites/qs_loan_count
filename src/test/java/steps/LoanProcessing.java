package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class LoanProcessing 
{
   WebDriver driver;
   CSVWriter writer;
   List<String[]> data;
   String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE4OCwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiw0MTRdLCJRUyJdLFsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIixbImZpZWxkLWlkIiwxMzE4XV0sIlBST0NFU1NJTkciXV0sImpvaW5zIjpbeyJmaWVsZHMiOiJhbGwiLCJzb3VyY2UtdGFibGUiOjE1OCwiY29uZGl0aW9uIjpbIj0iLFsiZmllbGQtaWQiLDQwNl0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgQ3VzdG9tdXNlciIsWyJmaWVsZC1pZCIsNzQ4XV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBDdXN0b211c2VyIn0seyJmaWVsZHMiOiJhbGwiLCJzb3VyY2UtdGFibGUiOjExMSwiY29uZGl0aW9uIjpbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEN1c3RvbXVzZXIiLFsiZmllbGQtaWQiLDc1MV1dLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFJlcXVpcmVkbG9hbiIsWyJmaWVsZC1pZCIsMTI5NF1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIn1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
   String MonitoringData[]= {"LoanProcessing","",""};
   public LoanProcessing(WebDriver driver,CSVWriter writer,List<String[]>data) throws InterruptedException, IOException
   {
	   this.driver=driver;
	    this.data=data;
	    Thread.sleep(2000);
	    ((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(8));
	    Thread.sleep(2000);
	    driver.get(Url);
	    Thread.sleep(2000);
	    MonitoringData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		Thread.sleep(2000);
		writer.writeNext(MonitoringData);
		writer.flush();
	   
   }
}
