package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class NachFormSigned 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]> data;
	String MonitoringData[]= {"NachFormSigned","",""};
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTg4LCJmaWx0ZXIiOlsiYW5kIixbInRpbWUtaW50ZXJ2YWwiLFsiZmllbGQtaWQiLDQxN10sLTEsImRheSIse31dLFsiPSIsWyJmaWVsZC1pZCIsNDE0XSwiUVMiXSxbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFJlcXVpcmVkbG9hbiIsWyJmaWVsZC1pZCIsMTMzN11dLHRydWVdXSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTU4LCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsNDA2XSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBDdXN0b211c2VyIixbImZpZWxkLWlkIiw3NDhdXV0sImFsaWFzIjoiTGVuZGVuYXBwIEN1c3RvbXVzZXIifSx7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTExLCJjb25kaXRpb24iOlsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgQ3VzdG9tdXNlciIsWyJmaWVsZC1pZCIsNzUxXV0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIixbImZpZWxkLWlkIiwxMjk0XV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBSZXF1aXJlZGxvYW4ifV0sImFnZ3JlZ2F0aW9uIjpbWyJjb3VudCJdXX0sImRhdGFiYXNlIjoyfSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	public NachFormSigned(WebDriver driver,CSVWriter writer,List<String[]>data) throws InterruptedException, IOException {
		this.driver=driver;
		this.data=data;
		Thread.sleep(2000);
		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(7));
	    Thread.sleep(2000);
        driver.get(Url);
        
        Thread.sleep(2000);
        MonitoringData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
        Thread.sleep(2000);
        writer.writeNext(MonitoringData);
        writer.flush();
	}
}
