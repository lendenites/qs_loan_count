package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class DeclinedQSLoan
{
	WebDriver driver;
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE4OCwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiw0MTRdLCJRUyJdLFsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIixbImZpZWxkLWlkIiwxMzE4XV0sIkRFQ0xJTkVEIl0sWyJ0aW1lLWludGVydmFsIixbImZpZWxkLWlkIiw0MTddLC0xLCJkYXkiLHt9XV0sImpvaW5zIjpbeyJmaWVsZHMiOiJhbGwiLCJzb3VyY2UtdGFibGUiOjE1OCwiY29uZGl0aW9uIjpbIj0iLFsiZmllbGQtaWQiLDQwNl0sWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgQ3VzdG9tdXNlciIsWyJmaWVsZC1pZCIsNzQ4XV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBDdXN0b211c2VyIn0seyJmaWVsZHMiOiJhbGwiLCJzb3VyY2UtdGFibGUiOjExMSwiY29uZGl0aW9uIjpbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEN1c3RvbXVzZXIiLFsiZmllbGQtaWQiLDc1MV1dLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFJlcXVpcmVkbG9hbiIsWyJmaWVsZC1pZCIsMTI5NF1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgUmVxdWlyZWRsb2FuIn1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	List<String[]> data;
	String MonitoringData[]= {"DeclinedLoanCount","",""};
public DeclinedQSLoan(WebDriver driver,CSVWriter writer,List<String[]> data) throws IOException, InterruptedException
{   this.data=data;
	this.driver=driver;
	Thread.sleep(2000);
	((JavascriptExecutor)driver).executeScript("window.open()");
    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(tabs.get(3));
    driver.get(Url);
    MonitoringData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
	//System.out.println("text is "+text);
    Thread.sleep(2000);
	writer.writeNext(MonitoringData);
	writer.flush();
	
}
}
