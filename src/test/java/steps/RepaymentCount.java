package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class RepaymentCount 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]>data;
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE1OCwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTg4LCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsNzQ4XSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBQcm9zcGVjdCIsWyJmaWVsZC1pZCIsNDA2XV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBQcm9zcGVjdCJ9LHsiZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoyMjEsImNvbmRpdGlvbiI6WyI9IixbImZpZWxkLWlkIiw3NTFdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIExvYW5hY2NvdW50IixbImZpZWxkLWlkIiwyNjU5XV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBMb2FuYWNjb3VudCJ9XSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBMb2FuYWNjb3VudCIsWyJmaWVsZC1pZCIsMjY2OV1dLCJSRVBBWU1FTlQiXSxbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIExvYW5hY2NvdW50IixbImZpZWxkLWlkIiwyNjY1XV0sIkNSRURJVCJdLFsidGltZS1pbnRlcnZhbCIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgTG9hbmFjY291bnQiLFsiZmllbGQtaWQiLDI2NjNdXSwtMSwiZGF5Iix7fV0sWyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBQcm9zcGVjdCIsWyJmaWVsZC1pZCIsNDE0XV0sIlFTIl1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	String MonitoringData[]= {"RepaymentCount","",""};
	public RepaymentCount(WebDriver driver,CSVWriter writer,List<String[]>data) throws InterruptedException, IOException
	{
		this.data=data;
		this.driver=driver;
		Thread.sleep(2000);
		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(9));
	    Thread.sleep(2000);
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
        Thread.sleep(2000);
		writer.writeNext(MonitoringData);
        writer.flush();    
	}

}
