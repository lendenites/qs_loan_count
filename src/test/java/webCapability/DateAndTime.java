package webCapability;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DateAndTime 
{
	SimpleDateFormat formatter,Timeformatter;
	int CurrentTime,preTime;
	WebElement elementStartDate,elementEndDate ,elementStartTime,elementEndTime;
	String CurrentDate,CurrentTimeForAllData,CurrentDateAndTime;
	WebDriver driver;
	Date date;
	public void setCurrentDateWithTime(WebDriver driver) throws Exception
	{
		this.driver=driver;
		date=new Date();
		formatter = new SimpleDateFormat("MM/dd/yyyy"); 
		CurrentDate=formatter.format(date);
		CurrentTime=date.getHours();

		if(CurrentTime==1)
		{
			preTime=23;
		}else if(CurrentTime==0)
		{
			preTime=22;
		}
		else {
			preTime=CurrentTime-2;
		}


		Thread.sleep(3000);
		elementStartTime=driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[1]/div/div[2]"));
		elementStartTime.click();
		elementStartTime=driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/input[1]"));
		elementStartTime.click();
		elementStartTime.sendKeys(Keys.CONTROL+"A");
		elementStartTime.sendKeys(Keys.BACK_SPACE);
		elementStartTime.sendKeys(""+preTime);
		if(preTime>=12)
		{
			driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/span[2]")).click();
		}else {
			driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/span[1]")).click();
		}

		Thread.sleep(2000);
		elementStartTime=driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[2]/div/div[2]"));
		elementStartTime.click();
		elementEndTime=driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div/input[1]"));
		elementEndTime.click();
		elementEndTime.sendKeys(Keys.CONTROL+"A");
		elementEndTime.sendKeys(Keys.BACK_SPACE);
		elementEndTime.sendKeys(""+CurrentTime);
		if(CurrentTime>=12)
		{
			driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div/div/span[2]")).click();
		}
		else 
		{
			driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div/div/span[1]")).click();
		}
		Thread.sleep(2000);
		elementStartDate= driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[1]/div/div[1]/input"));                                   
		elementStartDate.click();
		elementStartDate.sendKeys(Keys.CONTROL+"A");
		elementStartDate.sendKeys(Keys.BACK_SPACE);
		elementStartDate.sendKeys(CurrentDate);

		Thread.sleep(2000);
		elementEndDate=driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/input"));
		elementEndDate.click();
		elementEndDate.sendKeys(Keys.CONTROL+"A");
		elementEndDate.sendKeys(Keys.BACK_SPACE);
		elementEndDate.sendKeys(CurrentDate);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
		Thread.sleep(2000);

	}

	public void selectCurrentDate()
	{
		
		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();			
	}

	public String getCurrentTime()

	{
		Timeformatter = new SimpleDateFormat("HH:mm:ss a");
		CurrentTimeForAllData=Timeformatter.format(date);
		return CurrentTimeForAllData; 
	}

	public String getCurrentDate()
	{
		return CurrentDate;
	}

	public String getCurrentDateAndTime()

	{
		Timeformatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
		CurrentDateAndTime=Timeformatter.format(date);
		return CurrentDateAndTime; 
	}

}
