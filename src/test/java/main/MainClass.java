package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.opencsv.CSVWriter;

import steps.CancelledLoanCOunt;
import steps.DeclinedQSLoan;
import steps.DisbursedLoanCount;
import steps.EnachSignedCount;
import steps.ListedLoanCount;
import steps.LoanProcessing;
import steps.Login;
import steps.NachFormSigned;
import steps.OpenQSLoan;
import steps.RepaymentCount;
import steps.TotalCountOfQSLoan;
import steps.UploadedSignNach;
import webCapability.ExcelFile;
import webCapability.WebCapability;

public class MainClass extends WebCapability
{public List<String[]> data=new ArrayList<String[]>();


CSVWriter writer;
ExcelFile excelFile=new ExcelFile();
WebDriver driver;
@BeforeTest
public void openchrome() throws Exception
{
	this.driver = WebCapability();
	this.writer=excelFile.ExcelSheet();
	driver.manage().window().maximize();
	
}
@Test(priority = 1)
public void login() throws InterruptedException
{
	new Login(driver);
}
@Test(priority = 2)
public void LoanCount() throws Exception
{
	new TotalCountOfQSLoan(driver, writer, data);
}
@Test(priority = 3)
public void cancelloan() throws IOException, InterruptedException
{
	new CancelledLoanCOunt(driver,writer,data);
}
@Test(priority = 4)
public void open() throws InterruptedException, IOException
{
	new OpenQSLoan(driver, writer, data);
}
@Test(priority = 5)
public void declined() throws IOException, InterruptedException
{
	new DeclinedQSLoan(driver, writer, data);
}
@Test(priority = 6)
public void disbursed() throws IOException, InterruptedException
{
	new DisbursedLoanCount(driver, writer, data);
}
@Test(priority = 7)
public void Enach() throws InterruptedException, IOException
{
	new EnachSignedCount(driver,writer,data);
}
@Test(priority = 8)
public void NachSigned() throws InterruptedException, IOException
{
	new UploadedSignNach(driver, writer, data);
}
@Test(priority = 9)
public void Nachformsigned() throws InterruptedException, IOException
{
	new NachFormSigned(driver, writer, data);
}
@Test(priority = 10)
public void LoanProcessing() throws InterruptedException, IOException
{
	new LoanProcessing(driver, writer, data);
}
@Test(priority = 11)
public void Repayment() throws InterruptedException, IOException
{
	new RepaymentCount(driver, writer, data);
}
@Test(priority = 12)
public void Listed() throws Exception
{
	new ListedLoanCount(driver, writer, data);
}
@AfterTest
public void CloseBrowser()
{
    writer.writeAll(data);
	driver.quit	();
}
}
